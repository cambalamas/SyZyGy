/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <vector>
#include <unordered_map>
#include "Light.hpp"
#include "../ogl/Shader.hpp"

class LightManager {

public:

	// Upload the three vectors to OpenGL context
	static void lightsToUBO();

	// Add a light to the lights list (by type)
	static void addLight(Light& light);

private:

	// Static
	LightManager();
	LightManager(LightManager &&);
	LightManager(const LightManager&);

	// Vars
	static float Ia;
	static std::vector<LightData> lights;

	//static std::vector<LightData> dirLights;
	//static std::vector<LightData> spotLights;
	//static std::vector<LightData> pointLights;

};
