/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include <string>
#include <iostream>
#include <glm/glm.hpp>
#define GLM_FORCE_RADIANS
#include <glm/gtx/transform.hpp>

//#include "../utils/Utils.hpp" // <-- gl before glew prolbem... �?
#include "../utils/GData.hpp"

class Camera {

public:

	// Parametrized constructor
	Camera(const std::string& mode,
		   const float& fov = 60.f,
		   const glm::vec3 &pos = glm::vec3(0.f));

	// Allow camera movement updating the view matrix
	void updateView();


private:

	// Camera props
	const float speed = 50.f;
	const float zNear = 0.001f;
	const float zFar = 1000000.f;


/// PROPERTIES ------------------------------------------------------------ //


	// Zoom
private:
	float fov = 60.f;
	float orthoZ = 0.009f;
public:
	inline void modZoom(const float& variation) {
		this->fov += glm::radians(variation);
		this->orthoZ += 0.0001f * variation;
	}


	// Movement
private:
	glm::vec3 front = glm::vec3(0.f);
	glm::vec3 right = glm::vec3(0.f);
	glm::vec3 up = glm::vec3(0.f);
	glm::vec3 pos = glm::vec3(0.f);
public:
	inline void moveFront() { this->pos -= this->front * GData::delta(this->speed); }
	inline void moveBack() { this->pos += this->front * GData::delta(this->speed); }
	inline void moveLeft() { this->pos -= this->right * GData::delta(this->speed); }
	inline void moveRight() { this->pos += this->right * GData::delta(this->speed); }
	inline void moveUp() { this->pos -= this->up * GData::delta(this->speed); }
	inline void moveDown() { this->pos += this->up * GData::delta(this->speed); }
	inline glm::vec3 getEye() const { return this->pos; }
	inline float getYaw() const { return this->yaw; }


	// Rotation
private:
	float pitch = 0.f; // X rotation angle
	float yaw = 45.f;   // Y rotation angle
public:
	inline void rotateY(const float& angle) {
		this->yaw += angle * GData::delta(this->speed) * 10.f;
	}
	inline void rotateX(const float& angle) {
		this->pitch -= angle * GData::delta(this->speed) * 10.f;
		if (this->pitch > 89.0f) { this->pitch = 89.0f; }
		if (this->pitch < -89.0f) { this->pitch = -89.0f; }
	}


	// Matrices
private:
	glm::mat4 view = glm::mat4(1.f);
	glm::mat4 proj = glm::mat4(1.f);
public:
	inline glm::mat4 getView() const { return this->view; }
	inline glm::mat4 getProj() const { return this->proj; }


	// Aspect Ratio
private:
	float windowW;
	float windowH;
	float aspectRatio = 1.0f;
public:
	inline void setAspectRatio(const int& w, const int& h) {
		this->windowW = static_cast<float>(w);
		this->windowH = static_cast<float>(h);
		this->aspectRatio = this->windowW / this->windowH;
	}


	// Projection type
private:
	std::string mode = "Perspective";
public:
	inline void setMode(const std::string& cameraMode) {
		this->mode = cameraMode;
		std::cout << "\tCurrent camera mode = " << cameraMode << std::endl;
	}
	inline std::string getMode() const { return this->mode; }

};
