/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <vector>
#include <string>
#include "Camera.hpp"

class CameraManager {

public:

	// Current active camera
	static Camera* active;

	// CameraManager loop
	static void loop();

	// Remove all cameras
	static void clear();

	// Append a new camera to the cameras array
	static void addCamera(Camera* c);

	// Modify active camera
	static void setActive(int index);

	// Replace the active camera with the next valid camera
	static void switchAtiveCamera();

	// Switch between "perspective" and "orthogonal"
	static void switchCameraMode();

	// Update the aspect ratio to all cameras
	static void updateAspectRatio(int w, int h);

private:

	// Static
	CameraManager();
	CameraManager(CameraManager&&);
	CameraManager(const CameraManager &);

	// Management vars
	static int camIndex;
	static std::string camMode;
	static std::vector<Camera*> cameras;
};
