/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <string>
#include <sstream>
#include "LightManager.hpp"
#include "../ogl/Error.hpp"

float LightManager::Ia = 0.3f;
std::vector<LightData> LightManager::lights;

void LightManager::lightsToUBO() {
	if (lights.size() > 0) {

		// Some vars
		int offset = 0;
		int nLights = lights.size();
		unsigned int lightBlockBuffer;
		int sizeOfData = 16 + (sizeof(LightData) * nLights);

		// 1) Create UBO
		GL_ASSERT(glGenBuffers(1, &lightBlockBuffer));
		GL_ASSERT(glBindBuffer(GL_UNIFORM_BUFFER, lightBlockBuffer));
		GL_ASSERT(glBufferData(GL_UNIFORM_BUFFER, sizeOfData, 0, GL_DYNAMIC_DRAW));

		// 2) Populate UBO
		GL_ASSERT(glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(float), &Ia));
		offset += sizeof(float);
		GL_ASSERT(glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(int), &nLights));
		offset += sizeof(int);
		offset += 8; //Padding
		GL_ASSERT(glBufferSubData(GL_UNIFORM_BUFFER, offset, (sizeof(LightData) * nLights), &lights[0]));
		offset += (sizeof(LightData) * nLights);

		// 3) Bind UBO
		GL_ASSERT(glBindBufferBase(GL_UNIFORM_BUFFER, 0, lightBlockBuffer)); // layout(binding = 0) uniform...
	}
}

void LightManager::addLight(Light& light) {
	lights.push_back(light.data());
}



