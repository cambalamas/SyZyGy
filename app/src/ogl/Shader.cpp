/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include "Shader.hpp"
#include "../env/CameraManager.hpp"
#include "Error.hpp"
#include <fstream>
#include <sstream>
#include "../utils/Utils.hpp"

Shader::Shader(const std::string& path, const std::unordered_map<std::string, float>& variables) {

	this->path = path;

	// 1) Create program where store the shaders
	unsigned int programID = glCreateProgram();

	// 2) Load shader and get Vertex and Fragment source
	ShaderSource shS = this->loadShader(path);

	// 2.1) Compute shader
	if (!shS.Compute.empty()) {
		unsigned int sCompute = this->compileShader(GL_COMPUTE_SHADER, shS.Compute);
		GL_ASSERT(glAttachShader(programID, sCompute));
		GL_ASSERT(glDeleteShader(sCompute))
	} else {
		// 2.2) Non-Optional shaders
		if (!shS.Vertex.empty()) {
			unsigned int sVertex = this->compileShader(GL_VERTEX_SHADER, shS.Vertex);
			GL_ASSERT(glAttachShader(programID, sVertex));
			GL_ASSERT(glDeleteShader(sVertex));
		}

		if (!shS.Fragment.empty()) {
			unsigned int sFragment = this->compileShader(GL_FRAGMENT_SHADER, shS.Fragment);
			GL_ASSERT(glAttachShader(programID, sFragment));
			GL_ASSERT(glDeleteShader(sFragment));
		}

		// 2.3) Optional shaders
		if (!shS.TessControl.empty() && !shS.TessEval.empty()) {
			this->tessellation = true;
			// Control
			unsigned int sTessControl = this->compileShader(GL_TESS_CONTROL_SHADER, shS.TessControl);
			GL_ASSERT(glAttachShader(programID, sTessControl));
			GL_ASSERT(glDeleteShader(sTessControl));
			// Eval
			unsigned int sTessEval = this->compileShader(GL_TESS_EVALUATION_SHADER, shS.TessEval);
			GL_ASSERT(glAttachShader(programID, sTessEval));
			GL_ASSERT(glDeleteShader(sTessEval));
		} else if (
			(shS.TessControl.empty() && !shS.TessEval.empty()) || (!shS.TessControl.empty() && shS.TessEval.empty())) {
			std::cerr << "SHADER :: A piece off tesellation is empty :(" << std::endl;
		}

		if (!shS.Geometry.empty()) {
			unsigned int sGeometry = this->compileShader(GL_GEOMETRY_SHADER, shS.Geometry);
			GL_ASSERT(glAttachShader(programID, sGeometry));
			GL_ASSERT(glDeleteShader(sGeometry));
		}
	}

	// 3) Link program to the current context
	GL_ASSERT(glLinkProgram(programID));

	// 4) Check for linking errors
	int linked;
	glGetProgramiv(programID, GL_LINK_STATUS, &linked);
	if (!linked) {
		this->programError(programID);
	}

	// 5) Store data
	this->path = path;
	this->variables = variables;
	this->program = programID;
}

void Shader::use() {
	GL_ASSERT(glUseProgram(this->program));

	// Time elapsed since app init
	glProgramUniform1i(
		this->program,
		this->getUniformID("_time"),
		glutGet(GLUT_ELAPSED_TIME));

	// Time deltaTime
	glProgramUniform1f(
		this->program,
		this->getUniformID("_deltaTime"),
		(float)Utils::deltaTime * 0.01f
	);

	// Update variables
	for (auto var : this->variables) {
		glProgramUniform1f(
			this->program,
			this->getUniformID(var.first.data()),
			var.second);
	}

	// Update textures
	for (auto tex : this->textures) {
		GL_ASSERT(glActiveTexture(GL_TEXTURE0 + tex.second));
		GL_ASSERT(glBindTexture(GL_TEXTURE_2D, tex.second));
		glProgramUniform1i(
			this->program,
			this->getUniformID(tex.first.data()),
			tex.second);
	}

	// Update preprocess textures
	for (auto pTex : this->preProcessTextures) {
		GL_ASSERT(glActiveTexture(GL_TEXTURE0 + pTex.second));
		GL_ASSERT(glBindTexture(GL_TEXTURE_2D, pTex.second));
		glProgramUniform1i(
			this->program,
			this->getUniformID(pTex.first.data()),
			pTex.second);
	}


	// Update camera eye
	glProgramUniform3fv(
		this->program,
		this->getUniformID("_eye"),
		1,
		&CameraManager::active->getEye()[0]);

	// Update camera yaw
	glProgramUniform1f(
		this->program,
		this->getUniformID("_yaw"),
		CameraManager::active->getYaw());

	// Update wireframe toggle
	glProgramUniform1i(
		this->program,
		this->getUniformID("_wireFrame"),
		GData::wireFrame);
}

unsigned int Shader::getUniformID(const char* tag) {
	return glGetUniformLocation(this->program, tag);

	// TODO :: Prompt error but only once !!!
	//if (u != -1) {
	//	return u;
	//} else {
	//	std::cerr << "SHADER (" << this->path << ")" << std::endl <<
	//		"\tUniform " << tag << " not found/used !" << std::endl;
	//	return -1;
	//}
}

float Shader::getUniform1f(const std::string& varStr) {
	return this->variables[varStr];
}

void Shader::setUniform1f(const std::string& varStr, const std::string& action, const float& value) {
	if (action == "Inc") {
		this->variables[varStr] += value;
	} else if (action == "Dec") {
		this->variables[varStr] -= value;
	} else if (action == "Toggle") {
		this->variables[varStr] = 1 - this->variables[varStr];
	}
	std::cout << "\t" << varStr << " :: " << this->variables[varStr] << std::endl;
}

ShaderSource Shader::loadShader(const std::string& path) {

	/*
	 * I prefer only one file for shaders so, parse one file to split
	 * into two source strings which will be vertex and fragment shaders.
	 */

	// Readable vars, not simply ints for types
	enum ShaderType {
		NONE = -1,
		COMPUTE,
		VERTEX,
		TESS_C,
		TESS_E,
		GEOMETRY,
		FRAGMENT
	};
	ShaderType type = ShaderType::NONE;
	std::stringstream ss[6];
	std::string line;

	// 1) Get the stream from a file in 'path' //TODO: Check path and prompt error
	std::ifstream shaderFile(path);

	// 2) Iter over its lines
	while (std::getline(shaderFile, line)) {
		// 2.1) Change shader type if '#shader' keyword appear
		if (line.find("#shader") != std::string::npos) {
			if (line.find("compute") != std::string::npos) {
				type = ShaderType::COMPUTE;
			} else if (line.find("vertex") != std::string::npos) {
				type = ShaderType::VERTEX;
			} else if (line.find("tessControl") != std::string::npos) {
				type = ShaderType::TESS_C;
			} else if (line.find("tessEval") != std::string::npos) {
				type = ShaderType::TESS_E;
			} else if (line.find("geometry") != std::string::npos) {
				type = ShaderType::GEOMETRY;
			} else if (line.find("fragment") != std::string::npos) {
				type = ShaderType::FRAGMENT;
			}
		}
		// 2.2) Put lines into a stream by type
		else {
			if (type != ShaderType::NONE)
				ss[(int)type] << line << '\n';
		}
	}

	// 3) Return the sources usign a struct to allow return two values
	return {
		ss[(int)ShaderType::COMPUTE].str(),
		ss[(int)ShaderType::VERTEX].str(),
		ss[(int)ShaderType::TESS_C].str(),
		ss[(int)ShaderType::TESS_E].str(),
		ss[(int)ShaderType::GEOMETRY].str(),
		ss[(int)ShaderType::FRAGMENT].str()
	};
}

unsigned int Shader::compileShader(const unsigned int& type, const std::string& source) {
	// 1) Get ID from OpenGL
	unsigned int shaderID = glCreateShader(type);

	// 2) Load the source as raw string
	const char* rawSource = source.c_str();
	GL_ASSERT(glShaderSource(shaderID, 1, &rawSource, nullptr));

	// 3) Compile shader
	GL_ASSERT(glCompileShader(shaderID));

	// 4) Check for compile errors
	int compiled;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		return this->shaderError(shaderID, type);
	}

	// 5) Return the ID of the shader
	return shaderID;
}
