#pragma once
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include "Shader.hpp"

class FBO {
public:
	FBO(const int& w, const int& h, Shader* shader);
	void resize(const int& w, const int& h);
	void bind();
	void draw();

	inline unsigned int getPosTex() const { return this->fboPos; }
	inline unsigned int getNormTex() const { return this->fboNorm; }
	inline unsigned int getColorTex() const { return this->fboColor; }
	inline unsigned int getDepthTex() const { return this->fboDepth; }

private:
	unsigned int fboID;
	unsigned int planeVAO;
	unsigned int fboPos, fboNorm, fboColor, fboDepth;

	Shader* shader;
	int planeVertex = 4;
	glm::vec3 blendColor;
	std::vector<float> plane = { -1.f, -1.f, 1.f, -1.f, -1.f,  1.f, 1.f,  1.f };
};
