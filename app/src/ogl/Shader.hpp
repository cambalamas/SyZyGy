/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <GL/glew.h>
#include <string>
#include <iostream>
#include <unordered_map>

struct ShaderSource {
	std::string Compute;
	std::string Vertex;
	std::string TessControl;
	std::string TessEval;
	std::string Geometry;
	std::string Fragment;
};

class Shader {
public:

	// Default constructor
	Shader() : program(0) {}

	// Parametrized constructor
	Shader(
		const std::string& path,
		const std::unordered_map<std::string, float>& variables = std::unordered_map<std::string, float>()
	);

	// Active the program of this shader
	void use();

	// Return the value for a new uniform if this exist
	unsigned int getUniformID(const char* tag);

	// Mod uniforms values
	void setUniform1f(const std::string & varStr, const std::string& action, const float & value);

	// Get uniform 1f value
	float getUniform1f(const std::string& varStr);

	// Uniforms to upload to GPU
	std::unordered_map<std::string, float> variables;

	// Uniforms textures
	std::unordered_map<std::string, unsigned int> textures;

	// Injected textures (from preprocess)
	std::unordered_map<std::string, unsigned int> preProcessTextures;

private:


	// Path of the file whose contains the code of shader
	std::string path;

	/** Actions to load a shader
	* @param path = where the shader file is
	* @result A struct with the source code of Vertex and Fragment shaders
	*/
	ShaderSource loadShader(const std::string& path);

	/** Compile shader and return the OpenGL program id
	* @param type == GL_VERTEX_SHADER || GL_FRAGMENT_SHADER
	* @param source == source code of the shader
	*/
	unsigned int compileShader(const unsigned int& type, const std::string& source);


/// "ERROR CHECKERS" ----------------------------------------------------- ///

	// Prompt errors during shader compiling
	inline unsigned int Shader::shaderError(const unsigned int& id, const unsigned int& type) {
		std::string typeStr;

		if (type == GL_COMPUTE_SHADER) typeStr = "COMPUTE";
		if (type == GL_VERTEX_SHADER) typeStr = "VERTEX";
		else if (type == GL_TESS_CONTROL_SHADER) typeStr = "TESS CONTROL";
		else if (type == GL_TESS_EVALUATION_SHADER) typeStr = "TESS EVALUATION";
		else if (type == GL_GEOMETRY_SHADER) typeStr = "GEOMETRY";
		else if (type == GL_FRAGMENT_SHADER) typeStr = "FRAGMENT";

		int len;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
		std::vector<char> msg(len);
		glGetShaderInfoLog(id, len, &len, &msg[0]);
		std::string sMsg(msg.data(), msg.size());
		std::cerr <<
			"*** There are errors on " << typeStr << " SHADER (" << this->path << ")."
			<< std::endl << sMsg << std::endl << std::endl;
		glDeleteShader(id);
		return 0;
	}

	// Prompt errors during program linking
	inline void Shader::programError(const unsigned int& id) {
		int len = 0;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
		std::vector<char> msg(len);
		glGetProgramInfoLog(id, len, &len, &msg[0]);
		std::string sMsg(msg.data(), msg.size());
		std::cerr <<
			"*** There are errors in PROGRAM (" << this->path << ")."
			<< std::endl << sMsg << std::endl << std::endl;
		glDeleteProgram(id);
	}


/// "PROPERTIES" --------------------------------------------------------- ///

	// Program
private:
	unsigned int program;
public:
	inline int getProgram() const { return this->program; }

	// has Tessellation
private:
	bool tessellation = false;
public:
	inline int hasTessellation() const { return this->tessellation; }

};
