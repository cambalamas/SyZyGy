#include <GL/glew.h>

#include "Error.hpp"
#include "FBO.hpp"
#include "../env/CameraManager.hpp"

FBO::FBO(const int& w, const int& h, Shader* shader) {

	// 1) Frame buffer
	GL_ASSERT(glGenFramebuffers(1, &this->fboID));
	GL_ASSERT(glBindFramebuffer(GL_FRAMEBUFFER, this->fboID));

	// 2) Plane
	unsigned int planeVBO;
	GL_ASSERT(glGenVertexArrays(1, &planeVAO));
	GL_ASSERT(glBindVertexArray(planeVAO));
	GL_ASSERT(glGenBuffers(1, &planeVBO));
	GL_ASSERT(glBindBuffer(GL_ARRAY_BUFFER, planeVBO));
	GL_ASSERT(glBufferData(GL_ARRAY_BUFFER, this->plane.size() * sizeof(float), &this->plane[0], GL_STATIC_DRAW));
	GL_ASSERT(glEnableVertexAttribArray(0));
	GL_ASSERT(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0));

	// 3) Position buffer
	GL_ASSERT(glGenTextures(1, &this->fboPos));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboPos));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	GL_ASSERT(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->fboPos, 0));

	// 4) Normal buffer
	GL_ASSERT(glGenTextures(1, &this->fboNorm));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboNorm));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	GL_ASSERT(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->fboNorm, 0));

	// 5) Color + specular buffer
	GL_ASSERT(glGenTextures(1, &this->fboColor));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboColor));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	GL_ASSERT(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, this->fboColor, 0));

	// 6) Depth buffer
	GL_ASSERT(glGenTextures(1, &this->fboDepth));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboDepth));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0));
	GL_ASSERT(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	GL_ASSERT(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	GL_ASSERT(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT));
	GL_ASSERT(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT));
	GL_ASSERT(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->fboDepth, 0));

	// 7) Buffer attachments
	unsigned int attachments[3] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2
	};
	GL_ASSERT(glDrawBuffers(3, attachments));

	// 8) Check error
	unsigned int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) { std::cerr << "POSTPROCESS :: " << status << std::endl; }

	// 9) UnBind !
	GL_ASSERT(glBindFramebuffer(GL_FRAMEBUFFER, 0));

	// 10) Set the shader to this postprocess step
	this->shader = shader;
}

void FBO::resize(const int & w, const int & h) {
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboPos));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, w, h, 0, GL_RGB, GL_FLOAT, nullptr));

	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboNorm));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, w, h, 0, GL_RGB, GL_FLOAT, nullptr));

	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboColor));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));

	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboDepth));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr));

	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, 0));
}

void FBO::bind() {
	GL_ASSERT(glBindFramebuffer(GL_FRAMEBUFFER, this->fboID));
	GL_ASSERT(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void FBO::draw() {
	//GL_ASSERT(glUseProgram(this->shader->getProgram()));
	this->shader->use();
	glUniformMatrix4fv(this->shader->getUniformID("matV"), 1, GL_FALSE, &(CameraManager::active->getView()[0][0]));

	GL_ASSERT(glActiveTexture(GL_TEXTURE0 + this->fboPos));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboPos));
	GL_ASSERT(glUniform1i(shader->getUniformID("fboPos"), this->fboPos));

	GL_ASSERT(glActiveTexture(GL_TEXTURE0 + this->fboNorm));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboNorm));
	GL_ASSERT(glUniform1i(shader->getUniformID("fboNorm"), this->fboNorm));

	GL_ASSERT(glActiveTexture(GL_TEXTURE0 + this->fboColor));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboColor));
	GL_ASSERT(glUniform1i(shader->getUniformID("fboColor"), this->fboColor));

	GL_ASSERT(glActiveTexture(GL_TEXTURE0 + this->fboDepth));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->fboDepth));
	GL_ASSERT(glUniform1i(shader->getUniformID("fboDepth"), this->fboDepth));

	//GL_ASSERT(glEnable(GL_BLEND));
	//GL_ASSERT(glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD));
	//GL_ASSERT(glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA));

	GL_ASSERT(glDisable(GL_CULL_FACE));
	GL_ASSERT(glDisable(GL_DEPTH_TEST));

	GL_ASSERT(glBindVertexArray(planeVAO));
	GL_ASSERT(glDrawArrays(GL_TRIANGLE_STRIP, 0, 4));

	GL_ASSERT(glEnable(GL_CULL_FACE));
	GL_ASSERT(glEnable(GL_DEPTH_TEST));
	//GL_ASSERT(glDisable(GL_BLEND));
}
