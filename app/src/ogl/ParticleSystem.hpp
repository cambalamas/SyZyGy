#pragma once

#include "Shader.hpp"
#include <windows.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <random>
#include "Error.hpp"
#include <chrono>

struct Distribution {
	typedef std::uniform_real_distribution<float> dF;
	dF x, y, z;
	Distribution() {}
	Distribution(glm::vec4 p, float d) {
		float d2 = d * 0.5f;
		x = dF(p.x - d2, p.x + d2);
		y = dF(p.y - d2, p.y + d2);
		z = dF(p.z - d2, p.z + d2);
	}
};


struct Particle {
	glm::vec4 currPos;
	glm::vec4 currVel;
	glm::vec4 initPos;
	glm::vec4 initVel;
	glm::vec4 color;
};


class ParticleSystem {
public:
	ParticleSystem(
		Shader* renderShader,
		Shader* computeShader,
		int maxLifeTime,
		glm::vec4 emitPos,
		std::vector<glm::vec4> attractors,
		float distribution,
		int particlesNum,
		int csWorkers
	);

	void draw();
	inline Shader* getRenderShader() const { return this->renderShader; }
	inline Shader* getComputeShader() const { return this->computeShader; }

private:
	unsigned int VAO;
	int maxLifeTime;
	std::vector<float> lifeTime;

	std::vector<Particle> particles;

	std::mt19937 mtNum;
	Distribution distr;

	Shader* renderShader;
	Shader* computeShader;

	glm::vec4 emitPos;
	std::vector<glm::vec4> attractors;
	float distribution;
	int particlesNum;
	int csWorkers;
};
