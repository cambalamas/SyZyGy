#pragma once

#include <vector>
#include<glm/glm.hpp>
#include "Error.hpp"
#include "../ogl/Shader.hpp"

class Grid {
public:

	Grid(Shader* renderShader, int cols, int rows, int scale, int baseHeight);
	void draw();

	inline Shader* getRenderShader() const { return renderShader; }


private:

	unsigned int VAO;
	unsigned int VBO;
	unsigned int EBO;
	Shader* renderShader;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> vertices;
	std::vector<unsigned int> indices;

	int cols = 0, rows = 0, scale = 0;



};
