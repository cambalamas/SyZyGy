﻿/**
 *
 * SyZyGy - Align your 3D ideas.
 * @author Daniel Camba Lamas <cambalamas@gmail.com>
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * ...
 *
 */

#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include "utils/Utils.hpp"
#include "io/Mouse.hpp"
#include "io/Display.hpp"
#include "io/Keyboard.hpp"
#include "3d/Scene.hpp"
#include "3d/Model.hpp"
#include "ogl/Shader.hpp"
#include "env/LightManager.hpp"
#include "env/CameraManager.hpp"
#include "ogl/FBO.hpp"
#include "ogl/Grid.hpp"

#define SCREEN_SIZE 640,768

int main(int argc, char **argv) {

/* ************************************************************************ */
//							  1) SZG SETTINGS
/* ************************************************************************ */

	// Allow accent marks
	std::locale::global(std::locale("spanish"));


/* ************************************************************************ */
//								2) CONTEXT
/* ************************************************************************ */

	glutInit(&argc, argv);
	glutInitContextVersion(4, 5);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);


/* ************************************************************************ */
//								3) WINDOW
/* ************************************************************************ */

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(SCREEN_SIZE);
	glutCreateWindow("SyZyGy - Align your 3D ideas");


/* ************************************************************************ */
//							  4) EXTENSIONS
/* ************************************************************************ */

	glewExperimental = GL_TRUE;
	GLenum initMsg = glewInit();
	if (initMsg != GLEW_OK) { std::cerr << glewGetErrorString(initMsg) << std::endl; }


/* ************************************************************************ */
//							   5) GL SETUP
/* ************************************************************************ */

	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glClearColor(1.f, 1.f, 1.f, 1.f);

	// Show the render device opengl version
	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << std::endl << "\t\t\t    [ Welcome to SyZyGy ]" << std::endl << std::endl;
	std::cout << "\t\tUsig OpenGL v" << oglVersion << std::endl << std::endl;


/* ************************************************************************ */
//							   6) HANDLERS
/* ************************************************************************ */

	// Mouse
	glutMouseFunc(Mouse::clic);
	glutMotionFunc(Mouse::motion);
	glutMouseWheelFunc(Mouse::wheel);

	// Display
	glutReshapeFunc(Display::reShape);
	glutDisplayFunc(Display::frameDraw); // <=== main loop

	// KeyBoard
	glutKeyboardFunc(Keyboard::keyboardReply);


/* ************************************************************************ */
//							 7) LOAD AND DRAW
/* ************************************************************************ */

	int config = 1;

	switch (config) {
		std::cout << "Loading config" << config << std::endl;

		case 0:
			Utils::config("res/configs/waterColor/config_AkuAku.json", SCREEN_SIZE);
			break;
		case 1:
			Utils::config("res/configs/waterColor/config_Cap.json", SCREEN_SIZE);
			break;
		case 2:
			Utils::config("res/configs/waterColor/config_Luigi.json", SCREEN_SIZE);
			break;
		case 3:
			Utils::config("res/configs/waterColor/config_Knucles.json", SCREEN_SIZE);
			break;

		default: std::cerr << "Unassigned config!" << std::endl; break;
	}

/* ************************************************************************ */
//						   8) ACTIVE GLUT LOOP
/* ************************************************************************ */

	glutMainLoop();
	return 0;
}


