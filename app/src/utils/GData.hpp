#pragma once

struct GData {
public:

	static float deltaTime;
	static bool wireFrame;
	static bool isPostprocessActive;
	static int activeConfig;

	static float delta(const float& input);

private:

	// Static
	GData();
	GData(GData&&);
	GData(const GData &);

};
