/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include <vector>
#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "../ogl/Shader.hpp"


struct Vertex {
	glm::vec3 Position = glm::vec3(0.f);      // layout(location = 0)
	glm::vec3 Normal = glm::vec3(0.f);        // layout(location = 1)
	glm::vec4 Color = glm::vec4(0.f);         // layout(location = 2)
	glm::vec2 TexCoord = glm::vec2(0.0f);     // layout(location = 3)
	glm::vec3 Tangent = glm::vec3(0.f);       // layout(location = 4)
	glm::vec3 BiTangent = glm::vec3(0.f);     // layout(location = 5)
};


struct Texture {
	GLuint id;
	std::string path;
	std::string type;
};


class Mesh {

public:

	Mesh(std::vector<GLuint> indices, std::vector<Vertex> vertices, std::vector<Texture> textures);
	void draw(Shader* shader);

private:

	GLuint VAO, VBO, EBO;
	std::vector<Vertex> vertices;
	std::vector<Texture> textures;
	std::vector<unsigned int> indices;

};
