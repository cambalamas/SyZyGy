/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <unordered_map>
#include "Mesh.hpp"
#include "../ogl/Error.hpp"


Mesh::Mesh(std::vector<GLuint> indices, std::vector<Vertex> vertices, std::vector<Texture> textures) {

	this->indices = indices;
	this->vertices = vertices;
	this->textures = textures;

	// 1) VAO
	GL_ASSERT(glGenVertexArrays(1, &this->VAO));
	GL_ASSERT(glBindVertexArray(this->VAO));

	// 2) VBOs :: Memory layout for structs is sequential for all its items.
	GL_ASSERT(glGenBuffers(1, &this->VBO));
	GL_ASSERT(glBindBuffer(GL_ARRAY_BUFFER, this->VBO));
	GL_ASSERT(glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW));

	// 3) EBO
	if (this->indices.size() > 0) {
		GL_ASSERT(glGenBuffers(1, &this->EBO));
		GL_ASSERT(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO));
		GL_ASSERT(glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(unsigned int), &this->indices[0], GL_STATIC_DRAW));
	}

	// 4) VERTEX ATTRIBS

	// layout(location = 0) | Positions
	unsigned int loc_Position = 0;
	GL_ASSERT(glEnableVertexAttribArray(loc_Position));
	GL_ASSERT(glVertexAttribPointer(loc_Position, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)0));

	// layout(location = 1) | Normal
	unsigned int loc_Normal = 1;
	GL_ASSERT(glEnableVertexAttribArray(loc_Normal));
	GL_ASSERT(glVertexAttribPointer(loc_Normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, Normal)));

	// layout(location = 2) | Color
	unsigned int loc_Color = 2;
	GL_ASSERT(glEnableVertexAttribArray(loc_Color));
	GL_ASSERT(glVertexAttribPointer(loc_Color, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, Color)));

	// layout(location = 3) | TexCoord
	unsigned int loc_TexCoord = 3;
	GL_ASSERT(glEnableVertexAttribArray(loc_TexCoord));
	GL_ASSERT(glVertexAttribPointer(loc_TexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, TexCoord)));

	// layout(location = 4) | Tangent
	unsigned int loc_Tangent = 4;
	GL_ASSERT(glEnableVertexAttribArray(loc_Tangent));
	GL_ASSERT(glVertexAttribPointer(loc_Tangent, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, Tangent)));

	// layout(location = 5) | BiTangent
	unsigned int loc_BiTangent = 5;
	GL_ASSERT(glEnableVertexAttribArray(loc_BiTangent));
	GL_ASSERT(glVertexAttribPointer(loc_BiTangent, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, BiTangent)));

	// 5) UNBIND VAO
	GL_ASSERT(glBindVertexArray(0));
}


void Mesh::draw(Shader* shader) {
	std::unordered_map<std::string, int> texsN; // N of texTypeN

	// 1) Bind textures
	for (unsigned int i = 0; i < this->textures.size(); i++) {
		// 1.1) Texture name on shader: pattern = tex|Type|N
		std::string type = this->textures[i].type;
		texsN[type] += 1;
		std::string sTex = type + std::to_string(texsN[type] - 1);

		// 1.2) Setup texture on OpenGL

		GL_ASSERT(glActiveTexture(GL_TEXTURE0 + this->textures[i].id));
		GL_ASSERT(glBindTexture(GL_TEXTURE_2D, this->textures[i].id));
		GL_ASSERT(glUniform1i(shader->getUniformID(sTex.data()), this->textures[i].id)); // TODO: change glunif... by glprogramunif...
	}

	// 2) Draw mesh
	GL_ASSERT(glBindVertexArray(this->VAO));
	if (!shader->hasTessellation()) {
		GL_ASSERT(glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0));
	} else {
		GL_ASSERT(glPatchParameteri(GL_PATCH_VERTICES, 3));
		GL_ASSERT(glDrawElements(GL_PATCHES, this->indices.size(), GL_UNSIGNED_INT, 0));
	}


	// 3) Un-Bind VAO
	GL_ASSERT(glBindVertexArray(0));
}
