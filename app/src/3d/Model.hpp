/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include <vector>
#include <iostream>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include "Mesh.hpp"
#include "../ogl/Shader.hpp"


struct Node {
	glm::mat4 transform;
	std::vector<Mesh> meshes;
};


class Model {

public:

	Model(const std::string& modelPath);

private:
	std::string dirPath;
	std::string modelPath;

	bool errPosition = false;
	bool errNormal = false;
	bool errColor = false;
	bool errTexCoord = false;
	bool errTangent = false;

	std::vector<Texture> loadedTexs; // Avoid duplicated loads

	// Actions to realize over a ASSIMP node
	void nodeTreat(const aiNode* node, const aiScene* scene);

	// Actions to realize over a ASSIMP mesh
	Mesh meshTreat(const aiMesh* mesh, const aiScene* scene);

	// Actions to realize over a ASSIMP material
	std::vector<Texture> mat2tex(const aiMaterial* mat,
								 const aiTextureType& type,
								 const std::string& typeName);

	// Print model errors
	void modelError(const char* error);


/// "PROPERTIES" -------------------------------------------------------- ///


	// Shader
private:
	Shader * shader;
public:
	inline Shader* getShader() const { return this->shader; }
	inline void setShader(Shader* shader) { this->shader = shader; }

	// Model Matrix
private:
	glm::mat4 modelMat = glm::mat4(1);
public:
	inline glm::mat4 getModelMat() const { return this->modelMat; }
	inline void setModelMat(const glm::mat4& modelMat) { this->modelMat = modelMat; }

	// Nodes
private:
	std::vector<Node> nodes;
public:
	inline std::vector<Node> getNodes() const { return this->nodes; }
};

