/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Scene.hpp"
#include "../ogl/Shader.hpp"
#include "../env/LightManager.hpp"

std::vector<Grid> Scene::grids;
std::vector<Model> Scene::models;
std::vector<FBO> Scene::preProcesses;
std::vector<FBO> Scene::postProcesses;
std::vector<ParticleSystem> Scene::particleSystems;

void Scene::drawGrids(int drawOnlyID) {
	glm::mat4 viewMat = CameraManager::active->getView();
	glm::mat4 projMat = CameraManager::active->getProj();
	glm::mat4 matVP = projMat * viewMat;

	if (drawOnlyID != -1) {
		auto g = grids.at(drawOnlyID);
		g.draw();
		glm::mat4 matN = glm::transpose(glm::inverse(viewMat));
		glUniformMatrix4fv(g.getRenderShader()->getUniformID("matVP"), 1, GL_FALSE, &(matVP[0][0]));
		glUniformMatrix4fv(g.getRenderShader()->getUniformID("matV"), 1, GL_FALSE, &(viewMat[0][0]));
		glUniformMatrix4fv(g.getRenderShader()->getUniformID("matP"), 1, GL_FALSE, &(projMat[0][0]));
		glUniformMatrix4fv(g.getRenderShader()->getUniformID("matN"), 1, GL_FALSE, &(matN[0][0]));
	} else {
		for (auto g : grids) {
			g.draw();
			glm::mat4 matN = glm::transpose(glm::inverse(viewMat));
			glUniformMatrix4fv(g.getRenderShader()->getUniformID("matVP"), 1, GL_FALSE, &(matVP[0][0]));
			glUniformMatrix4fv(g.getRenderShader()->getUniformID("matV"), 1, GL_FALSE, &(viewMat[0][0]));
			glUniformMatrix4fv(g.getRenderShader()->getUniformID("matP"), 1, GL_FALSE, &(projMat[0][0]));
			glUniformMatrix4fv(g.getRenderShader()->getUniformID("matN"), 1, GL_FALSE, &(matN[0][0]));
		}
	}
}

void Scene::drawParticles() {
	glm::mat4 viewMat = CameraManager::active->getView();
	glm::mat4 projMat = CameraManager::active->getProj();
	glm::mat4 matVP = projMat * viewMat;

	for (auto ps : particleSystems) {
		ps.draw();
		glUniformMatrix4fv(ps.getRenderShader()->getUniformID("matVP"), 1, GL_FALSE, &(matVP[0][0]));
		glUniformMatrix4fv(ps.getRenderShader()->getUniformID("matV"), 1, GL_FALSE, &(viewMat[0][0]));
		glUniformMatrix4fv(ps.getRenderShader()->getUniformID("matP"), 1, GL_FALSE, &(projMat[0][0]));
	}
}

void Scene::computeScene() {

	// 1a) Treat the postprocessing files
	if (postProcesses.size() > 0 && GData::isPostprocessActive) {

		// 1a.1) Forward rendering to the first FBO
		postProcesses.at(0).bind();
		//drawParticles();
		drawModels();
		//drawGrids();

		// 1a.2) Concatenate all postprocesses
		for (unsigned int i = 1; i < postProcesses.size(); i++) {
			postProcesses.at(i).bind();
			postProcesses.at(i - 1).draw();
		}

		// 1a.3) Blend the last postprocess to the screen
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		postProcesses.back().draw();
	}

	// 1b) If not postprocessing, draw directly
	else {
		//drawParticles();
		drawModels();
		//drawGrids();
	}
}

void Scene::drawModels() {
	glm::mat4 viewMat = CameraManager::active->getView();
	glm::mat4 projMat = CameraManager::active->getProj();

	for (const Model& model : models) {
		Shader* currShader = model.getShader();
		glm::mat4 modelMat = model.getModelMat();

		currShader->use();

		for (const Node& node : model.getNodes()) {

			static float angle;
			angle = (angle > 2.0f * 3.14159) ? 0.0f : angle + 0.001f;

			// 1) Apply the model matrix of the modelObject to its nodes
			//glm::mat4 nodeTransform = modelMat;
			glm::mat4 auxNodeTransform = modelMat;
			auxNodeTransform = glm::rotate(auxNodeTransform, angle, glm::vec3(0, 1, 0));
			glm::mat4 nodeTransform = auxNodeTransform * node.transform;

			// 2) Compute matrices for current model
			glm::mat4 matMV = viewMat * nodeTransform;
			glm::mat4 matVP = projMat * viewMat;
			glm::mat4 matMVP = projMat * matMV;
			glm::mat4 matN = glm::transpose(glm::inverse(matMV));

			// 3) Upload matrices to the shader
			/// [!] : get IDs (matMV, matMVP, ...) from a separated file?
			// ModelView
			glUniformMatrix4fv(currShader->getUniformID("matMV"), 1, GL_FALSE, &(matMV[0][0]));
			// ViewProjection
			glUniformMatrix4fv(currShader->getUniformID("matVP"), 1, GL_FALSE, &(matVP[0][0]));
			// ModelViewProjection
			glUniformMatrix4fv(currShader->getUniformID("matMVP"), 1, GL_FALSE, &(matMVP[0][0]));
			// Normals
			glUniformMatrix4fv(currShader->getUniformID("matN"), 1, GL_FALSE, &(matN[0][0]));
			// View
			glUniformMatrix4fv(currShader->getUniformID("matV"), 1, GL_FALSE, &(viewMat[0][0]));
			// Model
			glUniformMatrix4fv(currShader->getUniformID("matM"), 1, GL_FALSE, &(nodeTransform[0][0]));
			// Projection
			glUniformMatrix4fv(currShader->getUniformID("matP"), 1, GL_FALSE, &(projMat[0][0]));

			// 4) Draw current model
			for (Mesh mesh : node.meshes) { mesh.draw(currShader); }
		}
	}
}

void Scene::addGrid(const Grid & grid) { grids.push_back(grid); }

void Scene::addModel(const Model& model) { models.push_back(model); }

void Scene::addParticleSystem(const ParticleSystem& ps) { particleSystems.push_back(ps); }

void Scene::addPostProcess(const FBO & postprocess) { postProcesses.push_back(postprocess); }

void Scene::postprocessAspect(const int & w, const int & h) {
	for (FBO& p : postProcesses) { p.resize(w, h); }
}
