/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <GL/freeglut.h>

class Mouse {

public:

	static void motion(int x, int y);
	static void clic(int button, int state, int x, int y);
	static void wheel(int wheel, int direction, int x, int y);

private:

	//Static
	Mouse();
	Mouse(Mouse &&);
	Mouse(const Mouse &);

	// Vars
	static float mouseX;
	static float mouseY;
	static bool motionFlag;

};
