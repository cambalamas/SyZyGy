/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include "Display.hpp"
#include "../3d/Scene.hpp"
#include "../ogl/Error.hpp"
#include "../env/CameraManager.hpp"
#include "../3d/Scene.hpp"
#include "../utils/Utils.hpp"
#include "../utils/GData.hpp"

#include <GL/freeglut.h>

int Display::frames = 0;
int Display::timeBase = 0;
bool Display::paused = false;
std::chrono::steady_clock::time_point Display::fpsBefore;
std::chrono::steady_clock::time_point Display::deltaBefore;

void Display::frameDraw() {

	// Time.deltaTime
	std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
	Utils::deltaTime = static_cast<float>(0.001f * std::chrono::duration_cast<std::chrono::milliseconds>(now - deltaBefore).count());
	deltaBefore = now;
	GData::deltaTime = Utils::deltaTime;

	// FPS
	frames++;
	int currenttime = glutGet(GLUT_ELAPSED_TIME);
	if (currenttime - timeBase > 1000) {
		double fps = (frames * 1000.0 / (currenttime - timeBase));
		std::string title = "SyZyGy - Align your 3D ideas  /  FPS: " + std::to_string(fps);
		glutSetWindowTitle(title.data());
		timeBase = currenttime;
		frames = 0;
	}

	// Render loop
	if (!paused) {
		Scene::computeScene();
		glutSwapBuffers();
		//paused = true;
		CameraManager::loop();
		GL_ASSERT(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	}

	// Efficient re-draw
	glutTimerFunc(1, reDraw, 0);
}

void Display::reShape(int w, int h) {
	glViewport(0, 0, w, h);
	Scene::postprocessAspect(w, h);
	CameraManager::updateAspectRatio(w, h);
}

void Display::reDraw(int value) {
	glutPostRedisplay();
}
