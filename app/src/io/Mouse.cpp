/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <iostream>

#include "Mouse.hpp"
#include "../env/CameraManager.hpp"

float Mouse::mouseX = 0;
float Mouse::mouseY = 0;
bool Mouse::motionFlag = false;

void Mouse::motion(int x, int y) {
	if (motionFlag) {
		// X mouse move == Y camera rotation
		float horizontal = (mouseX - static_cast<float>(x)) * 0.0001f;
		CameraManager::active->rotateY(horizontal);
		// Y mouse move == X camera rotation
		float vertical = (mouseY - static_cast<float>(y)) * 0.0001f;
		CameraManager::active->rotateX(vertical);
	}
}

void Mouse::clic(int button, int state, int x, int y) {
	//std::cout << "MOUSE button ";
	//// Button
	//if (button == GLUT_LEFT_BUTTON) std::cout << "Left ";
	//else if (button == GLUT_MIDDLE_BUTTON) std::cout << "Center ";
	//else if (button == GLUT_RIGHT_BUTTON) std::cout << "Right ";
	//// Status
	//if (state == GLUT_DOWN) std::cout << "Pressed ";
	//else if (state == GLUT_UP) std::cout << "Released ";
	//// Position
	//std::cout << "at (" << x << ", " << y << ")" << std::endl;

	// Left clic enter
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON) {
		mouseX = static_cast<float>(x);
		mouseY = static_cast<float>(y);
		motionFlag = true;
	} else if (state == GLUT_UP && button == GLUT_LEFT_BUTTON) {
		motionFlag = false;
	}
}

void Mouse::wheel(int wheel, int direction, int x, int y) {
	CameraManager::active->modZoom(static_cast<float>(-direction));
}
