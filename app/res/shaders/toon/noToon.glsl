#shader vertex
#version 440 core

// DATA ================================================================= //

layout(location=0) in vec3 meshPos;
layout(location=1) in vec3 meshNormal;
layout(location=2) in vec4 meshColor;
layout(location=3) in vec2 meshTexCoord;
layout(location=4) in vec3 meshTangents;
layout(location=5) in vec3 meshBiTangents;

uniform mat4 matMV;
uniform mat4 matMVP;
uniform mat4 matN;

out vec3 vPos;
out vec3 vNorm;
out vec2 vTexCoord;

// VERT MAIN ============================================================= //

void main() {
	vPos = (matMV * vec4(meshPos, 1)).xyz;
	vNorm = (matN * vec4(meshNormal, 0)).xyz;
	vTexCoord = meshTexCoord;
	gl_Position = matMVP * vec4(meshPos, 1);
}


// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---


#shader fragment
#version 440 core

// DATA ================================================================== //

in vec3 vPos;
in vec3 vNorm;
in vec2 vTexCoord;
uniform mat4 matV;
uniform sampler2D texDiffuse0;
layout(location = 0) out vec4 glFragColor;

vec3 N;
vec3 V;
vec3 Ka;
vec3 Kd;
vec3 Ks;
float Roughness;

// LIGHTS ================================================================= //

struct LightData {
	vec4 position;
	vec4 color;
	vec3 attenuation;
	float angle;
	vec3 direction;
	float intensity;
};

uniform int lightsNumber;
uniform float ambientIntensity;
layout(binding = 0) uniform _LIGHTS { LightData LIGHTS[]; };

float attenuation(in vec3 att, in vec3 L) {
	float LD = length(L);
	return 1.0 / max((att.x + att.y*LD + att.z*LD*LD), 0.1);
}

vec3 specular(in vec3 L) {
    vec3 R = reflect(-L,N);
    vec3 phong = Ks * pow(max(dot(R,V),0.0001), Roughness);
    return phong;
}

vec3 diffuse(in vec3 L) {
	return clamp(Kd * dot(N, L), 0, 1);
}

vec3 light(in LightData _light) {
	vec3 L = normalize((matV * _light.position).xyz - vPos);
    float Att = attenuation(_light.attenuation, L);
	vec3 ICA = _light.intensity * _light.color.rgb * Att;
    return ICA * ( diffuse(L) + specular(L) );
}

// FRAG MAIN ============================================================== //

void main()
{
    // Init globals
	N = normalize(vNorm);
	V = normalize(-vPos);
	Ka = vec3(1.,0.,1.);
	Kd = vec3(1.,0.,0.);
	Ks = vec3(1.);
	Roughness = 5.; // More roughness => Less specular shine

    // Shade
	vec3 shade = ambientIntensity * Ka;
	for (int i = 0; i < lightsNumber; i++) { shade += light(LIGHTS[i]); }

    // Fog
    vec3 clearColor = vec3(0.25);
	float fogD = length(vPos);
	float fogF = 1 / exp(0.01 * fogD * fogD);
	shade = fogF * shade + (1 - fogF) * clearColor;

    // * OUT * //
    glFragColor = vec4(shade,1.);
}
