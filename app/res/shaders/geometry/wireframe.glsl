#shader vertex
#version 440 core
uniform mat4 matMVP;
layout(location = 0) in vec3 meshPos;
void main() { gl_Position = matMVP * vec4(meshPos, 1); }

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader geometry
#version 440 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

vec4 N;

void emitLineStrip(int a, int b) {
    gl_Position = gl_in[a].gl_Position + N*0.01;
    EmitVertex();
    gl_Position = gl_in[b].gl_Position + N*0.01;
    EmitVertex();
    EndPrimitive();
}

void main()
{
    // Normal
    vec4 AB = gl_in[0].gl_Position - gl_in[1].gl_Position;
    vec4 CB = gl_in[2].gl_Position - gl_in[1].gl_Position;
    N = normalize(vec4( cross(AB.xyz, CB.xyz), 0 ));

    // Edges
    emitLineStrip(0,1); // AB
    emitLineStrip(1,2); // BC
    emitLineStrip(2,0); // CA
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core
layout(location = 0) out vec4 glFragColor;
void main() { glFragColor = vec4(1.0, 1.0, 0.0, 1.0); }
