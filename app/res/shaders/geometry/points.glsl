#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;

uniform mat4 matMV;

void main() { gl_Position = matMV * vec4(meshPos, 1); }

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader geometry
#version 440 core

layout (triangles) in;
layout (points, max_vertices = 1) out;

uniform mat4 matP;
uniform float Magnitude;

void main()
{
    // Normal
    vec4 AB = gl_in[0].gl_Position - gl_in[1].gl_Position;
    vec4 CB = gl_in[2].gl_Position - gl_in[1].gl_Position;
    vec4 N = normalize(vec4( cross(AB.xyz, CB.xyz), 0 ));

    // Points
    for (uint idx=0; idx < 3; idx++) {
        gl_Position = matP * (gl_in[idx].gl_Position + N*0.5);
        gl_PointSize = Magnitude;
        EmitVertex();
        EndPrimitive();
    }
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core
layout(location = 0) out vec4 glFragColor;
void main() { glFragColor = vec4(1,1,0,1); }
