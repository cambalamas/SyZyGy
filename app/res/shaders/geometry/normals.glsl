#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;

uniform mat4 matN;
uniform mat4 matMV;

out vec4 vNorm;

void main() {
	gl_Position = matMV * vec4(meshPos, 1);
    vNorm = normalize((matN * vec4(meshNormal, 0)));
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader geometry
#version 440 core

layout (triangles) in;
layout (line_strip, max_vertices = 3) out;

in vec4 vNorm[];

uniform mat4 matP;
uniform float Magnitude;

void main()
{
    for (uint idx=0; idx < 3; idx++) {
        gl_Position = matP * gl_in[idx].gl_Position;
        EmitVertex();
        gl_Position = matP * (gl_in[idx].gl_Position + vNorm[idx] * Magnitude);
        EmitVertex();
        EndPrimitive();
    }
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core
layout(location = 0) out vec4 glFragColor;
void main() { glFragColor = vec4(1,1,0,1); }
