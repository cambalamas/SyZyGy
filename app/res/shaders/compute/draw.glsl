#shader vertex
#version 440 core
layout(location = 0) in vec4 currPos;
layout(location = 1) in vec4 color;
uniform mat4 matVP;
out vec4 vColor;
void main() {
	vColor = color;
	gl_Position = matVP * currPos;
	gl_PointSize = 2.;
}

#shader fragment
#version 440 core
in vec4 vColor;
out vec4 glFragColor;
void main() { glFragColor = vec4(1.0); }
