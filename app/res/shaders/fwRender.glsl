#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;
layout(location = 2) in vec4 meshColor;
layout(location = 3) in vec2 meshTexCoord;
layout(location = 4) in vec3 meshTangents;
layout(location = 5) in vec3 meshBiTangents;

out vec3 vPos;
out vec3 vNorm;
out vec2 vTexCoord;

uniform mat4 matN;
uniform mat4 matMV;
uniform mat4 matMVP;

void main() {
	vPos = (matMV * vec4(meshPos, 1)).xyz;
	vNorm = (matN * vec4(meshNormal, 0)).xyz;
	vTexCoord = meshTexCoord;

	gl_Position = matMVP * vec4(meshPos, 1);
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core

in vec3 vPos;
in vec3 vNorm;
in vec2 vTexCoord;

layout(location = 0) out vec4 glFragColor;

uniform mat4 matV;
uniform sampler2D texDiffuse0;

// Global vars

float Roughness;
vec3 N, V, Kd;

// Lights

struct LightData {
	vec4 position;
	vec4 color;
	vec3 attenuation;
	float angle;
	vec3 direction;
	float intensity;
};
layout(binding = 0) uniform _LIGHTS{
	float Ia;
	int nLights;
	LightData LIGHTS[10];
};
vec3 light(in LightData _light) {

	// Light data
	vec3 L = normalize((matV * _light.position).xyz - vPos);
	float LD = length(L); // For attenuation
	vec3 H = normalize(L + V); // For blinn phong
	float I = _light.intensity;
	vec3 C = _light.color.rgb;

	// Attenuation
	vec3 attF = _light.attenuation;
	float A = 1.0 / max((attF.x + attF.y*LD + attF.z*LD*LD), 1.0);

	// Object color
	vec3 diffuse = clamp(Kd * dot(N, L), 0, 1);
	vec3 specular = vec3(1.) * pow(max(dot(N, H), 0.0001), Roughness);

	// Contribution of light to the object
	return I * C * A * (diffuse + specular);
}


// EntryPoint

void main() {
	// 3D model properties
	Roughness = 50.0;
	V = normalize(-vPos);
	N = normalize(vNorm);
	Kd = texture(texDiffuse0, vTexCoord).rgb;

	if (Kd == vec3(0)) { Kd = vec3(1.0, 0.5, 0.2); }

	// Shade
	vec3 shade = vec3(0.75) * Kd;
	for (int i = 0; i < nLights; i++) { shade += light(LIGHTS[i]); }

	// Fog
	// vec3 clearColor = vec3(0.25);
	// float fogD = length(vPos);
	// float fogF = 1 / exp(0.01 * fogD * fogD);
	// shade = fogF * shade + (1 - fogF) * clearColor;

	// Output
	glFragColor = vec4(shade, 1.);
}
