#shader vertex
#version 440 core

layout(location=0) in vec3 meshPos;
layout(location=1) in vec3 meshNorm;
layout(location=3) in vec2 meshTexCoord;

out vec3 vPos;
out vec3 vNorm;
out vec2 vTexCoord;

uniform mat4 matM;

void main() {
	vPos = (matM * vec4(meshPos,1)).xyz;
	vNorm = normalize(meshNorm);
    vTexCoord = meshTexCoord;
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader tessControl
#version 440 core

layout(vertices = 3) out;

in vec3 vPos[];
in vec3 vNorm[];
in vec2 vTexCoord[];

out vec3 tcPos[];
out vec3 tcNorm[];
out vec2 tcTexCoord[];

uniform float tessIn;
uniform float tessOut;

void main() {

    #define ID gl_InvocationID

    // Pass values
    tcPos[ID] = vPos[ID];
    tcNorm[ID] = vNorm[ID];
    tcTexCoord[ID] = vTexCoord[ID];

    // Write levels only once
    if (ID == 0) {
        gl_TessLevelOuter[0] = tessOut; // Triangle edge AB
        gl_TessLevelOuter[1] = tessOut; // Triangle edge BC
        gl_TessLevelOuter[2] = tessOut; // Triangle edge CA
        gl_TessLevelInner[0] = tessIn; // Triangle inside (baricentric point)
    }

    #undef ID
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader tessEval
#version 440 core

layout(triangles, equal_spacing, ccw) in;

in vec3 tcPos[];
in vec3 tcNorm[];
in vec2 tcTexCoord[];

out vec3 tePos;
out vec3 teNorm;
out vec2 teTexCoord;
out vec3 tePatchDist;

uniform mat3 matN;
uniform mat4 matVP;

uniform sampler2D texDispl;
uniform float DisplMagnitude;

void main() {

    // tex coords interpolation
    teTexCoord = gl_TessCoord.z * tcTexCoord[0] +
                 gl_TessCoord.x * tcTexCoord[1] +
                 gl_TessCoord.y * tcTexCoord[2];

    // displacement
    vec4 Displacement = texture(texDispl, teTexCoord);

    // for draw "wireframe"
    tePatchDist = gl_TessCoord;

    // ---

    // triangle initial vertices
    vec3 p1 = tcPos[0];
    vec3 p2 = tcPos[1];
    vec3 p3 = tcPos[2];

    // UVW
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    float w = gl_TessCoord.z;

    // ---

    // normals
    vec3 n1 = tcNorm[0];
    vec3 n2 = tcNorm[1];
    vec3 n3 = tcNorm[2];

    // normals angle
    float v12 = (2.*(dot(p2 - p1, n1 + n2) / dot(p2 - p1, p2 - p1)));
    float v23 = (2.*(dot(p3 - p2, n2 + n3) / dot(p3 - p2, p3 - p2)));
    float v31 = (2.*(dot(p1 - p3, n3 + n1) / dot(p1 - p3, p1 - p3)));

    // new normals
    vec3 n200 = n1;
    vec3 n020 = n2;
    vec3 n002 = n3;
    vec3 n110 = normalize(n1 + n2 - v12*(p2 - p1));
    vec3 n011 = normalize(n2 + n3 - v23*(p3 - p2));
    vec3 n101 = normalize(n3 + n1 - v31*(p1 - p3));

    // normal
    teNorm = n200*(w*w) + n020*(u*u) + n002*(v*v) +
             n110*w*u + n011*u*v + n101*w*v;

    // ---

    // vertex curvature angle
    float w12 = dot(p2-p1, n1);
    float w21 = dot(p1-p2, n2);
    float w13 = dot(p3-p1, n1);
    float w31 = dot(p1-p3, n3);
    float w23 = dot(p3-p2, n2);
    float w32 = dot(p2-p3, n3);

    // new vertices (following PN Triangles)
    vec3 b300 = p1;
    vec3 b030 = p2;
    vec3 b003 = p3;
    vec3 b210 = (2.*p1 + p2 - w12*n1) * 0.333;
    vec3 b120 = (2.*p2 + p1 - w21*n2) * 0.333;
    vec3 b021 = (2.*p2 + p3 - w23*n2) * 0.333;
    vec3 b012 = (2.*p3 + p2 - w32*n3) * 0.333;
    vec3 b102 = (2.*p3 + p1 - w31*n3) * 0.333;
    vec3 b201 = (2.*p1 + p3 - w13*n1) * 0.333;
    vec3 ee = (b210 + b120 + b021 + b012 + b102 + b201) * 0.166;
    vec3 vv = (p1+p2+p3) * 0.333;
    vec3 b111 = ee + (ee - vv) * 0.5;

    // position (by bezier)
    tePos = b300*(w*w*w)    + b030*(u*u*u)    + b003*(v*v*v)    +
		    3.*b210*u*(w*w) + 3.*b120*(u*u)*w + 3.*b201*v*(w*w) +
            3.*b021*(u*u)*v + 3.*b102*(v*v)*w + 3.*b012*u*(v*v) +
            6.*b111*u*v*w;

    // gl position (with displacement "map")
    if(Displacement.g > 0.4 && Displacement.r > 0.4)
        gl_Position = matVP * vec4(tePos + teNorm * DisplMagnitude, 1);
    else
        gl_Position = matVP * vec4(tePos, 1);

}


//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core

in vec3 teNorm;
in vec2 teTexCoord;
in vec3 tePatchDist;
out vec4 glFragColor;
uniform sampler2D texDispl;

void main() {
    vec3 color = texture(texDispl, teTexCoord).rgb;
    glFragColor = vec4(color, 1.0);
}
