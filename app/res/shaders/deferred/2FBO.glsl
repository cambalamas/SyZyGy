#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;
layout(location = 3) in vec2 meshTexCoord;

out vec3 vPos;
out vec3 vNorm;
out vec2 vTexCoord;

uniform mat4 matN;
uniform mat4 matMV;
uniform mat4 matMVP;

void main() {
	vTexCoord = meshTexCoord;
	vPos = (matMV * vec4(meshPos, 1)).xyz;
	vNorm = (matN * vec4(meshNormal, 0)).xyz;
	gl_Position = matMVP * vec4(meshPos, 1);
}


// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---


#shader fragment
#version 440 core

in vec3 vPos;
in vec3 vNorm;
in vec2 vTexCoord;

layout(location = 0) out vec3 fboPos;
layout(location = 1) out vec3 fboNorm;
layout(location = 2) out vec4 fboColor;

uniform sampler2D texDiffuse0;

void main() {
	fboPos = vPos;
	fboNorm = normalize(vNorm);
	//fboColor = vec4(vec3(1.), 5.);

	// slope based color
	float slope = normalize(fboPos).y;
	vec2 Hs = vec2(0.1, 0.8);
	vec3 cB = vec3(0.1, 0.9, 0.2);
	vec3 cM = vec3(0.5, 0.9, 0.3);
	vec3 cU = vec3(0.9, 0.9, 0.9);
	if (slope < Hs.x) {
		float blend = slope / Hs.x;
		fboColor.xyz = mix(cB, cM, blend);
	}
	if ((slope < Hs.y) && (slope >= Hs.x)) {
		float blend = (slope - Hs.x) * (1.0 / (Hs.y - Hs.x));
		fboColor.xyz = mix(cM, cU, blend);
	}
	if (slope >= Hs.y) { fboColor.xyz = cU; }
}
